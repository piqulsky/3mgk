from flask import Flask
from flask_restful import Resource, Api, request
from flask_mysqldb import MySQL

import datetime
import json
import base64
import os
import re

app = Flask(__name__)

app.config['MYSQL_HOST'] = 'localhost'
app.config['MYSQL_USER'] = 'root'
app.config['MYSQL_PASSWORD'] = ''
app.config['MYSQL_DB'] = 'website'

mysql = MySQL(app)

api = Api(app)

class Albums_All(Resource):
    def get(self):
        cursor = mysql.connection.cursor()
        cursor.execute("SELECT * FROM albums")
        data = cursor.fetchall()
        result = []
        for i in data:
            temp = dict((("album_id", i[0]), ("name", i[1]), ("create_time", i[2].strftime('%Y-%m-%d %H:%M:%S'))))
            result.append(temp)
        return result
    def post(self):
        get_json_items = request.get_json()
        name = get_json_items.get("name")
        conn = mysql.connection
        cursor = conn.cursor()
        cursor.execute(f"INSERT INTO `albums` (`album_id`, `name`, `create_time`) VALUES ('', '{name}', '{datetime.datetime.now().isoformat()}')")
        try:
            conn.commit()
            return "added"
        except:
            return "false"

api.add_resource(Albums_All, '/album')

class Albums(Resource):
    def get(self, id):
        cursor = mysql.connection.cursor()
        cursor.execute(f"SELECT * FROM albums WHERE album_id={id}")
        data = cursor.fetchall()
        result = []
        for i in data:
            temp = dict((("album_id", i[0]), ("name", i[1]), ("create_time", i[2].strftime('%Y-%m-%d %H:%M:%S'))))
            result.append(temp)
        return result
    def post(self):
         get_json_items = request.get_json()
         name = get_json_items.get("name")
         conn = mysql.connection
         cursor = conn.cursor()
         cursor.execute(f"INSERT INTO `albums` (`album_id`, `name`, `create_time`) VALUES ('', '{name}', '{datetime.datetime.now().isoformat()}')")
    def put(self, id):
        get_json_items = request.get_json()
        conn = mysql.connection
        cursor = conn.cursor()
        name = get_json_items.get("name")
        cursor = mysql.connection.cursor()
        for i in range(len(name)):
            if (name[i] == "\0" or name[i] == "`"):
                return "SQLinjection detected"
            i+=1
        cursor.execute(f"UPDATE `albums` SET `name` = '{name}' WHERE `albums`.`album_id` = {id}")    
        try:
            conn.commit()
            return "updated"
        except:
            return "false"
    def delete(self, id):
        conn = mysql.connection
        cursor = conn.cursor()
        cursor.execute(f"DELETE FROM `albums` WHERE `albums`.`album_id` = {id}")
        try:
            conn.commit()
            return "deleted"
        except:
            return "false"

api.add_resource(Albums, '/album/<id>')

class PostImage(Resource):
     def post(self,album_id):
        get_json_items = request.get_json()
        author = get_json_items.get("author")
        # img_data = bytes(get_json_items.get("photo"), 'UTF-8')
        img_data = get_json_items.get("photo")
        img_data = bytes((re.sub('data:image/jpeg;base64,', '', img_data) and re.sub('data:image/png;base64,', '', img_data)), "UTF-8")

        for i in range(len(author)):
            if (author[i] == "\0" or author[i] == ""):
                return "SQLinjection detected"
            i+=1

        source = os.path.dirname(os.path.abspath(__file__))+"\\images\\"
        source = source.replace("\\", "/")
        print(source)
        count = 0
        for path in os.listdir(source):
            count += 1
        source = f'{rf"{source}"+str(count)}.png'
        
        f = open(source, 'wb')
        f.write(base64.urlsafe_b64decode(img_data))
        f.close()

        conn = mysql.connection    
        cursor = mysql.connection.cursor()
        cursor.execute(f"INSERT INTO `photos` (`photo_id`, `author`, `source`, `create_time`, `album_id`) VALUES (NULL, '{author}', '{source}', '{datetime.datetime.now().isoformat()}', '{album_id}')")
        try:
            conn.commit()
            return "added "+source
        except:
            return "false"

api.add_resource(PostImage, '/album/image/<album_id>')

class Photos_getByAlbum(Resource):
    def get(self, album_id):
        cursor = mysql.connection.cursor()
        cursor.execute(f"SELECT * FROM `photos` WHERE `album_id` = {album_id}")
        data = cursor.fetchall()
        result = []
        for i in data:
            temp = dict((("photo_id", i[0]), ("author", i[1]), ("source", i[2]), ("create_time", i[3].strftime('%Y-%m-%d %H:%M:%S')), ("album_id", i[4])))
            result.append(temp)
        return result

api.add_resource(Photos_getByAlbum, '/image/album/<album_id>')

class Photos(Resource):
    def get(self, id):
        cursor = mysql.connection.cursor()
        cursor.execute(f"SELECT * FROM `photos` WHERE `photo_id` = {id}")
        data = cursor.fetchall()
        result = []
        for i in data:
            temp = dict((("photo_id", i[0]), ("author", i[1]), ("source", i[2]), ("create_time", i[3].strftime('%Y-%m-%d %H:%M:%S')), ("album_id", i[4])))
            result.append(temp)
        return result
    def post(self, id):
        get_json_items = request.get_json()
        author = get_json_items.get("author")
        content = get_json_items.get("content")

        for i in range(len(author)):
            if (author[i] == "\0" or author[i] == "`"):
                return "SQLinjection detected"
            i+=1
        for j in range(len(content)):
            if (content[j] == "\0" or content[j] == "`"):
                return "SQLinjection detected"
            j+=1

        conn = mysql.connection
        cursor = conn.cursor()
        cursor.execute(f"INSERT INTO `comments` (`comment_id`, `author`, `create_time`, `content`, `photo_id`) VALUES ('', '{author}', '{datetime.datetime.now().isoformat()}', '{content}', '{id}')")
        try:
            conn.commit()
            return "added"
        except:
            return "false"
    def delete(self, id):
        conn = mysql.connection
        cursor = conn.cursor()
        cursor.execute(f'DELETE FROM `photos` WHERE `photo_id` = {id}')
        try:
            conn.commit()
            return "deleted"
        except:
            return "false"

api.add_resource(Photos, '/image/<id>')

class Comments_getByPhoto(Resource):
    def get(self, photo_id):
        cursor = mysql.connection.cursor()
        cursor.execute(f"SELECT * FROM `comments` WHERE `photo_id` = {photo_id}")
        data = cursor.fetchall()
        result = []
        for i in data:
            temp = dict((("comment_id", i[0]), ("author", i[1]), ("create_time", i[2].strftime('%Y-%m-%d %H:%M:%S')), ("content", i[3]), ("photo_id", i[4])))
            result.append(temp)
        return result

api.add_resource(Comments_getByPhoto, '/comment/image/<photo_id>')

class Comment(Resource):
    def get(self, id):
        cursor = mysql.connection.cursor()
        cursor.execute(f"SELECT * FROM comments WHERE comment_id = {id}")
        data = cursor.fetchall()
        result = []
        for i in data:
            temp = dict((("comment_id", i[0]), ("author", i[1]), ("create_time", i[2].strftime('%Y-%m-%d %H:%M:%S')), ("content", i[3]), ("photo_id", i[4])))
            result.append(temp)
        return result
    def post(self, id):
        get_json_items = request.get_json()
        author = get_json_items.get("author")
        content = get_json_items.get("content")

        for i in range(len(author)):
            if (author[i] == "\0" or author[i] == "`"):
                return "SQLinjection detected"
            i+=1
        for j in range(len(content)):
            if (content[j] == "\0" or content[j] == "`"):
                return "SQLinjection detected"
            j+=1

        conn = mysql.connection
        cursor = conn.cursor()
        cursor.execute(f'INSERT INTO `replies` (`reply_id`, `author`, `create_time`, `content`, `comment_id`) VALUES (NULL, \'{author}\', \'{datetime.datetime.now().isoformat()}\', \'{content}\', {id});')
        try:
            conn.commit()
            return "added"
        except:
            return "false"
    def put(self, id):
        get_json_items = request.get_json()
        content = get_json_items.get("content")

        for j in range(len(content)):
            if (content[j] == "\0" or content[j] == "`"):
                return "SQLinjection detected"
            j+=1

        conn = mysql.connection
        cursor = conn.cursor()
        cursor.execute(f'UPDATE comments SET content = "{content}" WHERE comment_id = {id}')
        try:
            conn.commit()
            return "updated"
        except:
            return "false"
    def delete(self, id):
        conn = mysql.connection
        cursor = conn.cursor()
        cursor.execute(f'DELETE FROM comments WHERE comments.comment_id = {id}')
        try:
            conn.commit()
            return "deleted"
        except:
            return "false"

api.add_resource(Comment, '/comment/<id>')

class Replies_getByComment(Resource):
    def get(self, comment_id):
        cursor = mysql.connection.cursor()
        cursor.execute(f"SELECT * FROM `replies` WHERE `comment_id` = {comment_id}")
        data = cursor.fetchall()
        result = []
        for i in data:
            temp = dict((("reply_ID", i[0]), ("author", i[1]), ("create_time", i[2].strftime('%Y-%m-%d %H:%M:%S')), ("content", i[3]), ("comment_id", i[4])))
            result.append(temp)
        return result

api.add_resource(Replies_getByComment, '/reply/comment/<comment_id>')

class Replies(Resource):
    def get(self, id):
        cursor = mysql.connection.cursor()
        cursor.execute(f"SELECT * FROM replies WHERE reply_id = {id}")
        data = cursor.fetchall()
        result = []
        for i in data:
            temp = dict((("reply_ID", i[0]), ("author", i[1]), ("create_time", i[2].strftime('%Y-%m-%d %H:%M:%S')), ("content", i[3]), ("comment_id", i[4])))
            result.append(temp)
        return result
    def put(self, id):
        get_json_items = request.get_json()
        content = get_json_items.get("content")

        for j in range(len(content)):
            if (content[j] == "\0" or content[j] == "`"):
                return "SQLinjection detected"
            j+=1

        conn = mysql.connection
        cursor = conn.cursor()
        cursor.execute(f'UPDATE replies SET content = "{content}" WHERE reply_id = {id}')
        try:
            conn.commit()
            return "updated"
        except:
            return "false"
    def delete(self, id):
        conn = mysql.connection
        cursor = conn.cursor()
        cursor.execute(f'DELETE FROM replies WHERE replies.reply_id = {id}')
        try:
            conn.commit()
            return "deleted"
        except:
            return "false"

api.add_resource(Replies, '/reply/<id>') 
# GET: /reply/comment_id=int
# PUT: /reply/reply_id=int&author=string&create_time=string(format daty:rrrr-mm-dd gg-mm-ss)&content=string
# DELETE: /reply/reply_id=int



if __name__ == '__main__':
    app.run(debug=True, host='localhost', port=5000)

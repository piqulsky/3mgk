@startuml
map Albums {
 album_id => int
 ---
 name => varchar (50)
 create_time => datetime
}

map Photos{
photo_id => int
---
 author => varchar (120)
 source => varchar (50)
 album_id => int
}

map Comments{
 comment_id => int
---
 author => varchar (120)
 create_time => timestamp
 content => varchar (500)
 photo_id => int
}

map Replies{
 reply_id => int
---
 author => varchar (120)
 create_time => timestamp
 content => varchar (500)
 comment_id => int
}

Albums --{ Photos 
Photos --{ Comments
Comments --{ Replies
@enduml